import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){

	//To check if their is already a logged in user. Change the button from "Enroll" to "Login" if the user is not logged in.
	const { user } = useContext(UserContext);

	//Allow us to gain access to methods that will redirect a user to different page after enrolling a course.
	const navigate = useNavigate();

	// "useParams" hook allows us to retrieve the courseId passed via URL.
	const { productId } = useParams();

	// Create state hooks to capture the information of a specific course and display it in our application.
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [orderCount, setOrderCount] = useState(1);
	const [price, setPrice] = useState(0);
	const [amount, setAmount] = useState(0);

	const order = (productId) => {
		console.log(productName);
		fetch(`${process.env.REACT_APP_API_URL}/users/order`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				orderCount: orderCount
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Thank you for ordering!",
					icon: "success",
					text: "You have successfully placed an order."
				})
				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setAmount(data.price);
		})

	}, [productId])

	useEffect(() =>{
		console.log(orderCount);
		setAmount(price*orderCount);

	}, [orderCount])

	const handleDecrement = (card_id) => {
		if(orderCount > 0){
			setOrderCount(orderCount-1)
		}
	}

	const handleIncrement = (card_id) => {
		setOrderCount(orderCount+1)

	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle className="mt-2">Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<div className="input-group">
								<button type="button" onClick={() => handleDecrement(productId)} className="input-group-text">-</button>
								<div className="form-control text-center">{orderCount}</div>
								<button type="button" onClick={() => handleIncrement(productId)} className="input-group-text">+</button>
							</div>
							<Card.Subtitle className="mt-2">Total Amount:</Card.Subtitle>
							<Card.Text>PhP {amount}</Card.Text>
							
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
								<Button variant="primary" size="lg" onClick={() => order(productId)}>Order</Button>
								:
								<Button as={Link} to="/login" variant="primary" size="lg">Login to Order</Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}