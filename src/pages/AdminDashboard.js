import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDashboard(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	//"fetchData()" wherein we can invoke if their is a certain change with the course.
	const fetchData = () =>{
		// Get all courses in the database
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(products => {
				return(
					<tr key={products._id}>
						<td>{products._id}</td>
						<td>{products.productName}</td>
						<td>{products.description}</td>
						<td>{products.price}</td>
						<td>{products.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// We use conditional rendering to set which button should be visible based on the course status (active/inactive)
								(products.isActive)
								?	
								 	// A button to change the course status to "Inactive"
									<Button variant="danger" size="sm" onClick ={() => archive(products._id, products.productName)}>Archive</Button>
								:
									<>
										{/* A button to change the course status to "Active"*/}
										<Button variant="success" size="sm" onClick ={() => unarchive(products._id, products.productName)}>Unarchive</Button>
										{/* A button to edit a specific course*/}
										<Button as={ Link } to={`/editProducts/${products._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the course inactive
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the course active
	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all courses.
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*A button to add a new course*/}
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button variant="success" size="lg" className="mx-2">Show Orders</Button>
			</div>
			<Table striped bordered hover>
			<thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
