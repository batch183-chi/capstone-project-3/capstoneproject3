import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home() {
	const data = {
		title: "Inay Po's Kitchen",
		content: "Exquisite Taste of Heirloom Recipe",
		destination: "/menu",
		label: "Order Now!"
	}

	return(
		<>
			<Banner data={data} />
			<Highlights />
		</>
	)
}