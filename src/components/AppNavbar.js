import { Link } from "react-router-dom";
import { useState, useContext } from "react";
import UserContext from "../UserContext";

import {Navbar, Nav, Container} from "react-bootstrap";

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/" >Inay Po's Kitchen</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto" defaultActiveKey="/">
		            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
		            {
		            	(user.isAdmin)
		            	?
		            	<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
		            	:
		            	<Nav.Link as={Link} to="/products" eventKey="/products">Menu</Nav.Link>
	            	}
		            {
		            	(user.id !== null)
		            	?
		            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
		            	:
		            		<>
		            			<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            			<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
		            		</>

		            }
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		</Navbar>
	);
}