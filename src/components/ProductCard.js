import { useState, useEffect } from "react";
import {Link} from "react-router-dom";
import {Card, Button} from "react-bootstrap";


export default function ProductCard({productProp}){


	// Deconstruct courseProp properties into their own variable
	const {_id, productName, description, price} = productProp;

	return(
		<Card className="p-3 my-3">
			<Card.Body>
				<Card.Title>
				    {productName}
				</Card.Title>
				<Card.Subtitle>
					Description:
				</Card.Subtitle>
				 <Card.Text>
				    {description}
				</Card.Text>
				<Card.Subtitle>
					Price: 
				</Card.Subtitle>
				<Card.Text>
				    {price}
				</Card.Text>
			{/*We will be able to select a specific course through its URL*/}
				<Button as={Link} to={`/products/${_id}`} variant="primary">
					Details
				</Button>

			</Card.Body>
		</Card>
	)
}