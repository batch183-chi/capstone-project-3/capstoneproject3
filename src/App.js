import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {UserProvider} from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import AdminDashboard from "./pages/AdminDashboard"
import AddProduct from "./pages/AddProduct"
import Home from "./pages/Home";
import Edit from "./pages/EditProduct";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Register from "./pages/Register";

import {Container} from "react-bootstrap";
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  // To update the User State upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      // set the user states values with the user details upon successful login
      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              //undefined
              id: null,
              isAdmin: null
            })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
            <Routes>
                <Route exact path ="/" element={<Home />} />
                <Route exact path ="/admin" element={<AdminDashboard />} />
                <Route exact path="/addProduct" element={<AddProduct />}/>
                <Route exact path ="/editProducts/:productId" element={<Edit />} />
                <Route exact path ="/register" element={<Register />} />
                <Route exact path ="/login" element={<Login />} />
                <Route exact path ="/logout" element={<Logout />} />
                <Route exact path ="/products" element={<Products />} />
                <Route exact path ="/products/:productId" element={<ProductView />} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>

  );
}

export default App;
